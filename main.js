const electron = require('electron');
var fs = require('fs');
var { app, dialog, BrowserWindow, Menu } = electron

const path = require('path')
const url  = require('url')
var content = " ";
let win
let filename = ""

function createWindow(){
    win = new BrowserWindow({width: 800, height:600}) 
    win.loadURL(url.format({
        pathname: path.join(__dirname, 'views/index.html'),
        protocol: 'file',
        slashes: true
    }))

    win.webContents.openDevTools()
}

function openFile() {

    dialog.showOpenDialog({ filters: [{ name: 'text', extensions: ['txt'] }] }, function (fileNames) {

        if (fileNames === undefined) return;
        filename = fileNames[0];
        fs.readFile(filename, 'utf-8', function (err, data) {
            win.webContents.send('file',{msg:data,name:filename});
        });
    });
}

function saveFile() {
        if (filename === undefined){
            dialog.showOpenDialog({ filters: [{ name: 'text', extensions: ['txt'] }] }, function (fileNames) {

                if (fileNames === undefined) return;
                filename = fileNames[0];
            });
        }
        win.webContents.send('save',{file:filename});
}

var showOpen = function () {
    dialog.showOpenDialog({ properties: ['openFile'], filters: [{ name: 'All', extensions: ['*'] }] });
};


const mainMenuTemplate = [
    {
        label: 'Archivo',
        submenu: [
            {
                label: 'Abrir',
                accelerator: process.platform == 'darwin' ? 'Alt+N' : 'Alt+N',
                click() {
                    openFile()
                }
            },
            {
                label: 'Cerrar',
                accelerator: process.platform == 'darwin' ? 'Alt+E' : 'Alt+E',
                click() {
                    win.webContents.send('close',{cont:""});
                }
            },
            {
                label: 'Crear Nuevo',
                accelerator: process.platform == 'darwin' ? 'Alt+O' : 'Alt+O',
                click() {
                    // You can obviously give a direct path without use the dialog (C:/Program Files/path/myfileexample.txt)
                    dialog.showSaveDialog((fileName) => {
                        if (fileName === undefined) {
                            console.log("You didn't save the file");
                            return;
                        }
                        win.webContents.send('nuevo',{texto:"",file:fileName});
                        // fileName is a string that contains the path and filename created in the save file dialog.  
                        fs.writeFile(fileName, content, (err) => {
                            if(err!=undefined){
                                console.log(err)
                            }
                        });
                        filename = fileName;
                    });
                }
            }, {
                type: 'separator'
            },
            {
                label: 'Guardar',
                accelerator: process.platform == 'darwin' ? 'Command+S' : 'Ctrl+S',
                click() {
                    saveFile()
                }
            },
            {
                label: 'Guardar como',
                accelerator: process.platform == 'darwin' ? 'Command+Shift+S' : 'Ctrl+Shift+S',
                click() {
                    // You can obviously give a direct path without use the dialog (C:/Program Files/path/myfileexample.txt)
                    dialog.showSaveDialog((fileName) => {
                        if (fileName === undefined) {
                            console.log("No se guardo el archivo");
                            return;
                        }
                        filename = fileName;
                        win.webContents.send('save',{file:filename});
                    });
                }
            }, {
                type: 'separator'
            },
            {
                label: 'Salir',
                accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
                click() {
                    app.quit();
                }
            },
        ]
    },
    {
        label: 'Build and Debug',
        submenu: [
            {
                label: 'Build',
                accelerator: process.platform == 'darwin' ? 'Command+F5' : 'Ctrl+F5',
                click() {
                }
            },
            {
                label: 'Debug',
                accelerator: process.platform == 'darwin' ? 'Command+F6' : 'Ctrl+F6',
                click() {
                }
            },
            {
                label: 'Run',
                accelerator: process.platform == 'darwin' ? 'Command+F7' : 'Ctrl+F7',
                click() {
                    win.webContents.send('run',{cont:filename})
                }
            }
        ]
    },
    {
        label: 'Edicion',
        submenu: [
          {role: 'undo',label:'Deshacer'},
          {role: 'redo',label:'Rehacer'},
          {type: 'separator',label:'Separador'},
          {role: 'cut',label:'Cortar'},
          {role: 'copy',label:'Copiar'},
          {role: 'paste',label:'Pegar'},
          {role: 'pasteandmatchstyle',label:'Pegar y Estilizar'},
          {role: 'delete',label:'Borrar'},
          {role: 'selectall',label:'Seleccionar todo'}
        ]
    },
    {
        role: 'window',
        label: 'Ventana',
        submenu: [
          {role: 'minimize'},
          {role: 'close'}
        ]
    }
]

var mainMenu = Menu.buildFromTemplate(mainMenuTemplate);

// Insertamos el menu en la aplicación
Menu.setApplicationMenu(mainMenu);

app.on('ready', createWindow)