

const electron = require('electron');

var app = require('electron').remote;
var dialog = app.dialog;
var path = require('path');
var fs = require('fs');
var { app, BrowserWindow, Menu } = electron

var ipcRenderer = require('electron').ipcRenderer;

let file ="";
let texto="";
let lexico = "";
let errores = "";
let erroressintactico = "";
let JsonArbol = "";
let erroressemantico = "";
let JsonArbolSem = "";
let content = document.getElementById("side-content");
//let editor = ace.edit("editor");

editor.session.selection.on('changeCursor', function(e) {
    var cursor = editor.selection.getCursor();
    document.getElementById('filcol').innerText = "fila: " + (cursor.row+1) + ", Columna: " + cursor.column + ".";
    //console.log("fila: " + (cursor.row+1) + ", Columna: " + cursor.column + ".")
});

ipcRenderer.on('file', function (event, store) {
    
    //document.getElementById('editor').value = store.msg;
    editor.setValue(store.msg,1);
    file = store.name
    lexico=""
    ps1 = document.getElementById('ps1');
    showcontent('lexico',ps1);
    errores = "";
    pf1 = document.getElementById('pf1');
    $(function() {
        $('#tree1').tree('destroy');
        $('#tree2').tree('destroy');
    });
    showcontentf('lexico',pf1);
    JsonArbol = "{}"
    //cambiotexto();
});

ipcRenderer.on('nuevo', function (event, store) {
    file = store.file;
    //document.getElementById('code').value = store.texto;
    editor.setValue("",1);
    ps1 = document.getElementById('ps1');
    showcontent('lexico',ps1);
    //cambiotexto();
});

ipcRenderer.on('save',function(event,store){
    fs.writeFile(store.file, editor.session.getValue(), function (err) {
       alert("Se guardo correctamente");
    });
});

ipcRenderer.on('close',function(event,store){
    
    lexico = "";
    editor.setValue(store.cont,1);
    ps1 = document.getElementById('ps1');
    showcontent('lexico',ps1);
    //cambiotexto();
});

ipcRenderer.on('run',function(event,store){
    const exec = require('child_process').execSync;
    //var executablePath = "ruby \"C:\\Users\\Cesar Reyes\\Documents\\UNI\\6o\\Compiladores\\ide\\lexico.rb\" " + '"' + store.cont + '"';
    if((store.cont == "" || store.cont ===undefined) && file == "")
    {
        alert("Guarde el archivo o abra uno existente para continuar");
        return;
    }
    // fs.writeFile(store.file, editor.session.getValue(), function (err) {
    //     alert("Se guardo correctamente");
    //  });
    var abrir = ""
    if(store.cont == "" || store.cont ===undefined)
    {
        abrir = file
    }
    else{
        abrir = store.cont
    }
    var resultados = path.join(__dirname + '/../resultados');
    console.log(resultados);
    var archivo = resultados + "\\erroressintactico.txt";
    if (fs.existsSync(archivo)) {
        fs.unlink(archivo, (err) => {
            if (err) {
                alert("Ocurrio un error" + err.message);
                console.log(err);
                return;
            }
            console.log("Borrado");
        });
    }

    archivo = resultados + "\\arbolsintactico.txt";
    if (fs.existsSync(archivo)) {
        fs.unlink(archivo, (err) => {
            if (err) {
                alert("Ocurrio un error" + err.message);
                console.log(err);
                return;
            }
            console.log("Borrado");
        });
    }
    archivo = resultados + "\\arbolsemant.txt";
    if (fs.existsSync(archivo)) {
        fs.unlink(archivo, (err) => {
            if (err) {
                alert("Ocurrio un error" + err.message);
                console.log(err);
                return;
            }
            console.log("Borrado");
        });
    }
    archivo = resultados + "\\erroressemantico.txt";
    if (fs.existsSync(archivo)) {
        fs.unlink(archivo, (err) => {
            if (err) {
                alert("Ocurrio un error" + err.message);
                console.log(err);
                return;
            }
            console.log("Borrado");
        });
    }
    var executablePath = "ruby ruby\\analisislexico.rb " + '"' + abrir + '"';
    var pathsintactico = "ruby ruby\\analisissintactico.rb " + '"' + resultados + '\\intern-lex.txt"';
    var pathsemantico = "ruby ruby\\analisissemantico.rb " + '"' + resultados + '\\intern-sint.txt"';
    var millisecondsToWait = 100;
    setTimeout(function(){
        res = exec(executablePath)
        console.log(res)
        fs.readFile(resultados + "\\resultados-lexico.txt", 'utf-8', function (err, data) {
            if (err) {
                alert("Ha ocurrido un error al leer el archivo :" + err.message);
                return;
            }
    
            lexico = data;
            ps1 = document.getElementById('ps1');
            showcontent('lexico',ps1);
        });
        fs.readFile(resultados + "\\errores-lexico.txt", 'utf-8', function (err, data) {
            if (err) {
                alert("Ha ocurrido un error al leer el archivo :" + err.message);
                return;
            }
    
            errores = data;
            pf1 = document.getElementById('pf1');
            $(function() {
                $('#tree1').tree('destroy');
            });
            showcontentf('lexico',pf1);
        });
    },millisecondsToWait);
    setTimeout(function() {
        res = exec(pathsintactico)
        console.log(res)
        fs.readFile(resultados + "\\erroressintactico.txt",'utf-8',function(err,data){
            if (err) {
                alert("Ha ocurrido un error al leer el archivo :" + err.message);
                return;
            }
            erroressintactico = data;
        })
        fs.readFile(resultados + "\\arbolsintactico.txt",'utf-8',function(err,data){
            if (err) {
                alert("Ha ocurrido un error al leer el archivo :" + err.message);
                return;
            }
            JsonArbol = data;
        })
    }, millisecondsToWait);
    
    setTimeout(function() {
        res = exec(pathsemantico)
        console.log(res)
        fs.readFile(resultados + "\\erroressemantico.txt",'utf-8',function(err,data){
            if (err) {
                alert("Ha ocurrido un error al leer el archivo :" + err.message);
                return;
            }
            erroressemantico = data;
        })
        fs.readFile(resultados + "\\arbolsemant.txt",'utf-8',function(err,data){
            if (err) {
                alert("Ha ocurrido un error al leer el archivo :" + err.message);
                return;
            }
            JsonArbolSem = data;
        })
        fs.readFile(resultados + "\\tabla-ha-ash.txt",'utf-8',function(err,data){
            if (err) {
                alert("Ha ocurrido un error al leer el archivo :" + err.message);
                return;
            }
            JsonTabla = data;
        })
    }, millisecondsToWait);

    // exec(executablePath, function(err, data) {
    //     if(err){
    //     console.error(err);
    //     return;
    //     }
    
    //     console.log(data.toString());
    // });
    
    
    
    
});

function nuevo(){
    file = "";
    lexico = "";
    editor.setValue(" ",1);
    //cambiotexto();
}

function abrir(){
    dialog.showOpenDialog(function (fileNames) {
        if (fileNames === undefined) {
            console.log("no se selecciono un archivo");
        } else {
            file = fileNames[0];
            readFile(file);
            lexico = ""
            editor.setValue("",1);
        }
    });
    
}

function guardar(){
    if (file) {
        saveChanges(file, editor.session.getValue());
    } else {
        dialog.showSaveDialog((fileName) => {
            if (fileName === undefined) {
                console.log("No se guardo el archivo");
                return;
            }else{
                fs.writeFile(fileName, editor.session.getValue(), function (err) {
                    alert("Se guardo correctamente");
                 });
            }
        });
    }
}

function saveChanges(filepath, content) {
    fs.writeFile(filepath, content, function (err) {
        if (err) {
            alert("Ha ocurrido un error actualizando el archivo" + err.message);
            console.log(err);
            return;
        }

        alert("El archivo ha sido guardado satisfactoriamente");
    });
}


function readFile(filepath) {
    fs.readFile(filepath, 'utf-8', function (err, data) {
        if (err) {
            alert("Ha ocurrido un error al leer el archivo :" + err.message);
            return;
        }

        texto = data;
        editor.setValue(texto,1);
        //cambiotexto();
        texto="";
        
    });
}

function showcontent(option,bttn){
    
    //TODO: Recuperar los resultados de las ejecuciones en ruby para mostrarlos en estas secciones PD: no sé como hacerlo (aún)
    switch(option){
        case 'lexico':
            content.hidden = false
            content.innerText = lexico;
            $(function() {
                $('#tree2').tree('destroy');
                $('#tree1').tree('destroy');
            });
            $('#tree2').hide();
            $('#tree1').hide();
        break;
        case 'sintactico':
        content.innerHTML = "";
        content.hidden = true
            var data = JSON.parse(JsonArbol);
            $(function() {
                $('#tree1').tree('destroy');
                $('#tree2').tree('destroy');
            });
            $(function() {
                $('#tree2').hide();
                $('#tree1').show();
                $('#tree1').tree({
                    data: data,
                    autoOpen: true
                });
            });
        break;
        case 'semantico':
        content.innerHTML = "";
        content.hidden = true
            var data = JSON.parse(JsonArbolSem);
            $(function() {
                $('#tree2').tree('destroy');
                $('#tree1').tree('destroy');
            });
            $(function() {
                $('#tree1').hide();
                $('#tree2').show();
                $('#tree2').tree({
                    data: data,
                    autoOpen: true
                });
            });
        break;
        case 'hash':
        $('#tree2').hide();
        $('#tree1').hide();
        content.hidden = false
        var data = JSON.parse(JsonTabla);
        content.innerHTML=json2table(data,"table");
        $(function() {
            $('#tree2').tree('destroy');
            $('#tree1').tree('destroy');
        });

        break;
        case 'inter':
        $('#tree2').hide();
        $('#tree1').hide();
        content.hidden = false
        content.innerText="CODIGO INTERMEDIO chi que chi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi\nchi";
        $(function() {
            $('#tree2').tree('destroy');
            $('#tree1').tree('destroy');
        });
        break;
        default:
        $('#tree2').hide();
        $('#tree1').hide();
        $(function() {
            $('#tree2').tree('destroy');
            $('#tree1').tree('destroy');
        });
        content.innerText="";
        break;
    }

    var elements = document.getElementsByClassName('sactive');
    if(elements.length > 0){
        elements[0].className = "";
    }
    bttn.className = "sactive";
    
}


function showcontentf(option,bttn){
    var content = document.getElementById("foot-content");
    //TODO: Recuperar los resultados de las ejecuciones en ruby para mostrarlos en estas secciones PD: no sé como hacerlo (aún)
    switch(option){
        case 'lexico':
        content.innerText = errores;
        break;
        case 'sintactico':
        content.innerText=erroressintactico;
        break;
        case 'semantico':
        content.innerText=erroressemantico;
        break;
        case 'res':
        content.innerText="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec placerat sit amet nisl sed finibus. Donec malesuada lectus massa, sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec placerat sit amet nisl sed finibus. Donec malesuada lectus massa, sit amet";
        break;
        default:
        content.innerText="AKBRON";
        break;
    }

    var elements = document.getElementsByClassName('factive');
    if(elements.length > 0){
        elements[0].className = "";
    }
    bttn.className = "factive";
    
}

// function cambiotexto(){
    
//     var lineasdiv = document.getElementById('lineas');
//     var conteo = document.getElementById('lineas').innerText.split("\n");
//     var lineas = document.getElementById('code').value.split("\n");
//     if(texto != "" && texto != document.getElementById('code').value)
//     {
//         document.getElementById('code').value = texto;
//         lineas = texto.split("\n");
//     }

//     if(conteo.length<lineas.length)
//     {
//         for(i=conteo.length+1;i<=lineas.length;i++)
//         {
//             lineasdiv.innerText += "\n" + i+":";
//         }
//          //+ lineas.length + ":";   
//     }
//     else if(lineas.length<conteo.length){
//         var numeros = "1:";
//         for (let index = 2; index <= lineas.length; index++) {
//             numeros += "\n" + index + ":";
//         }
//         lineasdiv.innerText = numeros;
//     }
// }
function json2table(json, classes) 
{
    var cols = Object.keys(json[0]);
    
    var headerRow = '';
    var bodyRows = '';
    
    classes = classes || '';
  
    function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
  
    cols.map(function(col) {
      headerRow += '<th>' + capitalizeFirstLetter(col) + '</th>';
    });
  
    json.map(function(row) {
      bodyRows += '<tr>';
  
      cols.map(function(colName) {
        bodyRows += '<td>' + row[colName] + '</td>';
      })
  
      bodyRows += '</tr>';
    });
  
    return '<table class="' +
           classes +
           '"><thead><tr>' +
           headerRow +
           '</tr></thead><tbody>' +
           bodyRows +
           '</tbody></table>';
}

function sincronizar(){
    var lineasdiv = document.getElementById('lineas');
    var area = document.getElementById('code');
    
    lineasdiv.scrollTop = area.scrollTop;
}

function codeclick(){
    var lineas = document.getElementById('code').value.split("\n");
    var cursorPosition = $('#code')[0].selectionStart;
    var original = cursorPosition;
    var columna = 0;
    var fila = 0;
    var i = 0;
    while(lineas[i].length<cursorPosition){
        cursorPosition = cursorPosition-lineas[i].length;
        i++;
        if(lineas.length != i){
            cursorPosition = cursorPosition - 1;
        }
    }
    fila = i+1;
    columna = cursorPosition;
    document.getElementById('filcol').innerText = "fila: " + fila + ", Columna: " + columna + ".";
}



//************************ */
