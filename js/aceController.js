//var editor = ace.edit("editor");
// editor.setTheme("ace/theme/monokai");
// editor.session.setMode("ace/mode/javascript");


//------------------------

window.define = window.define || ace.define;


define('ace/mode/custom', [], function(require, exports, module) {
    var oop = require("ace/lib/oop");
    var TextMode = require("ace/mode/text").Mode;
    var Tokenizer = require("ace/tokenizer").Tokenizer;
    var CustomHighlightRules = require("ace/mode/custom_highlight_rules").CustomHighlightRules;
    
    var Mode = function() {
        this.HighlightRules = CustomHighlightRules;
    };
    oop.inherits(Mode, TextMode);
    
    (function() {
    
    }).call(Mode.prototype);
    
    exports.Mode = Mode;
    });
    
    define('ace/mode/custom_highlight_rules', [], function(require, exports, module) {
    var oop = require("ace/lib/oop");
    //var DocCommentHighlightRules = require("./doc_comment_highlight_rules").DocCommentHighlightRules;
    var TextHighlightRules = require("ace/mode/text_highlight_rules").TextHighlightRules;
    
    var CustomHighlightRules = function() {
    
        var keywordMapper = this.createKeywordMapper({
            "variable.language": "main|if|then|else|end|do|while|repeat|until|read|write|float|integer|bool",
            "keyword":
                "",
            "constant.language":
                "true|false|null"
        }, "text", true);

        // var Numeric = this.createKeywordMapper({
        //     "numeric": "1|2|3|4|5|6|7|8|9|0",
        // },"numero", true )
        
    
        // this.$rules = {
        //     "start": [            
        //         {
        //             regex: "\\w+\\b",
        //             token: keywordMapper,
        //         },{
        //             token : "constant.numeric", // decimal integers and floats
        //             regex : "[+-]?\\d+(?:(?:\\.\\d*)?(?:[eE][+-]?\\d+)?)?\\b"
        //         }
        //     ]
        // };

        this.$rules = {
            "start" : [
                {
                    token : "comment",
                    regex : "\\/\\/.*$"
                },
                //DocCommentHighlightRules.getStartRule("doc-start"),
                {
                    token : "comment", // multi line comment
                    regex : "\\/\\*",
                    next : "comment"
                 },
                 { 
                    token: 'punctuation.definition.string.begin.shell',
                    regex: '"',
                    push: [ 
                    { 
                        token: 'punctuation.definition.string.end.shell', regex: '"', next: 'pop'
                    },
                    { 
                        include: 'variable' 
                    },
                    { 
                        defaultToken: 'string.quoted.double.dosbatch' 
                    } ] 
                }, 
                //{
                //     token : "string", // character
                //     regex : /'(?:.|\\(:?u[\da-fA-F]+|x[\da-fA-F]+|[tbrf'"n]))?'/
                // }, {
                //     token : "string", start : '"', end : '"|$', next: [
                //         {token: "constant.language.escape", regex: /\\(:?u[\da-fA-F]+|x[\da-fA-F]+|[tbrf'"n])/},
                //         {token: "invalid", regex: /\\./}
                //     ]
                // }, {
                //     token : "string", start : '@"', end : '"', next:[
                //         {token: "constant.language.escape", regex: '""'}
                //     ]
                // }, {
                //     token : "string", start : /\$"/, end : '"|$', next: [
                //         {token: "constant.language.escape", regex: /\\(:?$)|{{/},
                //         {token: "constant.language.escape", regex: /\\(:?u[\da-fA-F]+|x[\da-fA-F]+|[tbrf'"n])/},
                //         {token: "invalid", regex: /\\./}
                //     ]
                // }, 
                {
                    token : "constant.numeric", // hex
                    regex : "0[xX][0-9a-fA-F]+"
                }, {
                    token : "constant.numeric", // float
                    regex : "[+-]?\\d+(?:(?:\\.\\d*)?(?:[eE][+-]?\\d+)?)?"
                }, {
                    token : "constant.language.boolean",
                    regex : "(?:true|false)"
                }, {
                    token : keywordMapper,
                    regex : "[a-zA-Z_$][a-zA-Z0-9_$]*"
                }, {
                    token : "keyword.operator",
                    regex : "!|\\$|%|@|&|\\||\\*|\\-\\-|\\-|\\+\\+|\\+|~|===|==|=|!=|!==|<=|>=|<<=|>>=|>>>=|<>|<|>|!|&&|\\|\\||\\?\\:|\\*=|%=|\\+=|\\-=|&=|\\^=|\\b(?:in|instanceof|new|delete|typeof|void)"
                }, {
                    token : "keyword",
                    regex : "^\\s*#(if|else|elif|endif|define|undef|warning|error|line|region|endregion|pragma)"
                }, {
                    token : "punctuation.operator",
                    regex : "\\?|\\:|\\,|\\;|\\."
                }, {
                    token : "paren.lparen",
                    regex : "[[({]"
                }, {
                    token : "paren.rparen",
                    regex : "[\\])}]"
                }, {
                    token : "text",
                    regex : "\\s+"
                }
            ],
            "comment" : [
                {
                    token : "comment", // closing comment
                    regex : "\\*\\/",
                    next : "start"
                }, {
                    defaultToken : "comment"
                }
            ]
        };
    
        this.normalizeRules()
    };
    
    oop.inherits(CustomHighlightRules, TextHighlightRules);
    
    exports.CustomHighlightRules = CustomHighlightRules;
    });
    

    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/monokai");
    editor.session.setMode("ace/mode/custom");
    editor.session.setFoldStyle('markbegin');
    // editor.setHighlightActiveLine(false);
    editor.setHighlightSelectedWord(false);
    editor.setHighlightGutterLine(false);

//--------------


