class Estados
    #objeto con las banderas para cad token
    def self.Id
        1
    end
    def self.Num
        2
    end
    def self.Suma
        3
    end
    def self.Resta
        4
    end
    def self.Menor
        5
    end
    def self.Mayor
        6
    end
    def self.Dosp
        7
    end
    def self.Nega
        8
    end
    def self.Igual
        9
    end
    def self.Mod
        10
    end
    def self.Diagonal
        11
    end
    def self.Cadena
        13
    end
    def self.Dec
        14
    end
    def self.Comenlin
        15
    end
    def self.Comenmul
        16
    end
    def self.multipcierre
        17
    end
    def self.multiplica
        18
    end
end

class Expresion
    def self.digito
        "1234567890"
    end
    def self.letra
        "abcdefghijklmnopqrstuvwxyz"
    end
    def self.post
        "*,;(){}"
    end
    def self.reservadas
        ["main", "if", "then", "else", "end", "do", "while", "repeat", "until", "read", "write"]
    end
    def self.tipos
        ["float", "integer", "bool"]
    end

    def self.booleanos
        ["true","false"]
    end
end

def buscar_reservada(palabra)
    Expresion.reservadas.each {|a|
      if (palabra == a)
        return true
      end
    }
    return false
end

def buscar_tipos(palabra)
    Expresion.tipos.each {|a|
      if (palabra == a)
        return true
      end
    }
    return false
end

def buscar_booleanos(palabra)
    Expresion.booleanos.each {|a|
      if (palabra == a)
        return true
      end
    }
    return false
end

def leer_archivo(nombre)
    #abre un archivo y regresa su contenido
    archivo = File.open(nombre)
    texto = archivo.read
    archivo.close
    return texto
  end

def main
  archivo = ARGV[0] #Recibe el nombre del archivo
  texto = leer_archivo(archivo)
  contador = 0
  estado = 0
  token = ""
  errores = ""
  analisis = ""
  intAnalisis = ""
  fila = 1
  col = -1
  finicio = 0
  cinicio = 0
  #Se recorre todo el contenido del archivo caracter por caracter
  while contador<texto.length
    caracter = texto[contador,1]

    contador += 1
    col += 1
    if caracter=="\n"
        col = 0
        fila +=1
    end
    if caracter == "\t"
        next
    end
    #revisando banderas
    case estado
    when Estados.Id
        if Expresion.letra.include?(caracter) || Expresion.digito.include?(caracter)
            token += caracter
        else
            estado = 0
            if(buscar_reservada(token))
                analisis += "#{token}  -> Palabra Reservada(#{finicio},#{cinicio}) \n"
                intAnalisis += "#{token}|reservada|(#{finicio},#{cinicio})#"
            elsif(buscar_tipos(token))
                analisis += "#{token}  -> Palabra Reservada (Tipo de dato)(#{finicio},#{cinicio}) \n"
                intAnalisis += "#{token}|tipo|(#{finicio},#{cinicio})#"
            elsif(buscar_booleanos(token))
                analisis += "#{token}  -> Palabra Reservada(#{finicio},#{cinicio}) \n"
                intAnalisis += "#{token}|bool|(#{finicio},#{cinicio})#"
            else
                analisis += "#{token}  -> Identificador(#{finicio},#{cinicio}) \n"
                intAnalisis += "#{token}|identificador|(#{finicio},#{cinicio})#"
            end
            token = ""
        end
    when Estados.Num
        if Expresion.digito.include?(caracter)
            token += caracter
        elsif caracter == "."
            estado = Estados.Dec
            token += caracter
        else
            estado = 0
            analisis += "#{token}  -> Numero entero (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|entero|(#{finicio},#{cinicio})#"
            token=""
        end
    when Estados.Dec
        if Expresion.digito.include?(caracter)
            token += caracter
        else
            estado = 0
            analisis += "#{token}  -> Numero decimal (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|decimal|(#{finicio},#{cinicio})#"
            token=""
        end
    when Estados.Suma
        if caracter == "+"
            estado = 0
            token+=caracter
            analisis += "#{token} -> Incremento (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|incremento|(#{finicio},#{cinicio})#"
            token=""
            next
        else
            estado = 0
            analisis += "#{token} -> Suma (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|suma|(#{finicio},#{cinicio})#"
            token=""
        end
    when Estados.Resta
        if caracter == "-"
            estado = 0
            token += caracter
            analisis += "#{token} -> Decremento (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|decremento|(#{finicio},#{cinicio})#"
            token = ""
            next
        else
            estado = 0
            analisis +="#{token} -> Resta (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|resta|(#{finicio},#{cinicio})#"
            token=""
        end
    when Estados.Menor
        if caracter == "="
            estado = 0
            token += caracter
            analisis += "#{token} -> Menor o igual (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|menori|(#{finicio},#{cinicio})#"
            token=""
            next
        else
            estado = 0
            analisis +="#{token} -> Menor que (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|menorq|(#{finicio},#{cinicio})#"
            token=""
        end
    when Estados.Mayor
        if caracter == "="
            estado = 0
            token += caracter
            analisis += "#{token} -> Mayor o igual (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|mayori|(#{finicio},#{cinicio})#"
            token=""
            next
        else
            estado = 0
            analisis +="#{token} -> Mayor que (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|mayorq|(#{finicio},#{cinicio})#"
            token=""
        end
    when Estados.Dosp
        if caracter == "="
            estado = 0
            token += caracter
            analisis += "#{token} -> Asignacion (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|asignacion|(#{finicio},#{cinicio})#"
            token=""
            next
        else
            estado = 0
            errores +="#{token} -> Error (#{finicio},#{cinicio})\n"
            token=""
        end
    when Estados.Nega
        if caracter == "="
            estado = 0
            token += caracter
            analisis += "#{token} -> Diferente (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|diferente|(#{finicio},#{cinicio})#"
            token=""
            next
        else
            estado = 0
            errores +="#{token} -> Error (#{finicio},#{cinicio})\n"
            token=""
        end
    when Estados.Igual
        if caracter == "="
            estado = 0
            token += caracter
            analisis += "#{token} -> Exactamente igual (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|igual|(#{finicio},#{cinicio})#"
            token=""
            next
        else
            estado = 0
            errores +="#{token} -> Error (#{finicio},#{cinicio})\n"
            token=""
        end
    when Estados.Diagonal
        if caracter == "/"
            estado = Estados.Comenlin
            token += caracter
        elsif caracter == "*"
            estado = Estados.Comenmul
            token += caracter
        else
            estado = 0
            analisis += "#{token}  -> Division (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|division|(#{finicio},#{cinicio})#"
            token=""
        end
    when Estados.Comenlin
        if caracter == "\n"
            estado = 0
            #analisis += "#{token}  -> Comentario en linea (#{finicio},#{cinicio})\n"
            token=""
            next
        else
            token += caracter
        end
    when Estados.Comenmul
        if caracter == "*"
            estado = Estados.multipcierre
            token += caracter
        else
            token += caracter
        end
    when Estados.multipcierre
        if caracter == "/"
            estado = 0
            token += caracter
            #analisis += "#{token} -> Comentario de multiple linea (#{finicio},#{cinicio})\n"
            token=""
            next
        else
            token += caracter
        end
    when Estados.Cadena
        if caracter == "\""
            estado = 0
            token += caracter
            analisis += "#{token} -> Cadena (#{finicio},#{cinicio})\n"
            intAnalisis += "#{token}|cadena|(#{finicio},#{cinicio})#"
            token=""
            next
        else
            token+=caracter
        end
    end

    #asignando estado si no hay token
    if estado == 0 && caracter != " " && caracter != "\n"
            if Expresion.letra.include?(caracter)
                estado = Estados.Id
                token += caracter
                finicio = fila
                cinicio = col
            elsif Expresion.digito.include?(caracter)
                estado  = Estados.Num
                token += caracter
                finicio = fila
                cinicio = col
            elsif caracter == "+"
                estado = Estados.Suma
                token += caracter
                finicio = fila
                cinicio = col
            elsif caracter == "-"
                estado = Estados.Resta
                token += caracter
                finicio = fila
                cinicio = col
            elsif caracter == "<"
                estado = Estados.Menor
                token += caracter
                finicio = fila
                cinicio = col
            elsif caracter == ">"
                estado = Estados.Mayor
                token += caracter
                finicio = fila
                cinicio = col
            elsif caracter == ":"
                estado = Estados.Dosp
                token += caracter
                finicio = fila
                cinicio = col
            elsif caracter == "!"
                estado = Estados.Nega
                token += caracter
                finicio = fila
                cinicio = col
            elsif caracter == "="
                estado = Estados.Igual
                token += caracter
                finicio = fila
                cinicio = col
            elsif caracter == "%"
                analisis += "#{caracter} -> Modulo(#{fila},#{col})\n"
                intAnalisis += "#{caracter}|modulo|(#{fila},#{col})#"
            elsif caracter == "/"
                estado = Estados.Diagonal
                token += caracter
                finicio = fila
                cinicio = col
            elsif caracter==";" || caracter==","
                analisis += "#{caracter} -> Separacion de variables (#{fila},#{col})\n"
                intAnalisis += "#{caracter}|separacion|(#{fila},#{col})#"
            elsif caracter=="."
                errores += "#{caracter} -> Error (#{fila},#{col})\n"
            elsif caracter=="("
                analisis += "#{caracter} -> Parentesis que abre (#{fila},#{col})\n"
                intAnalisis += "#{caracter}|parentesis abre|(#{fila},#{col})#"
            elsif caracter==")"
                analisis += "#{caracter} -> Parentesis que cierra (#{fila},#{col})\n"
                intAnalisis += "#{caracter}|parentesis cierra|(#{fila},#{col})#"
            elsif caracter=="{"
                analisis += "#{caracter} -> Llave que abre (#{fila},#{col})\n"
                intAnalisis += "#{caracter}|llave abre|(#{fila},#{col})#"
            elsif caracter=="}"
                analisis += "#{caracter} -> Llave que cierra (#{fila},#{col})\n"
                intAnalisis += "#{caracter}|llave cierra|(#{fila},#{col})#"
            elsif caracter == "*"
                analisis += "#{caracter} -> Multiplicacion (#{fila},#{col})\n"
                intAnalisis += "#{caracter}|multiplicacion|(#{fila},#{col})#"
            elsif caracter == "\""
                estado = Estados.Cadena
                token += caracter
                finicio = fila
                cinicio = col
            else
                errores += "#{caracter} -> Error(#{fila},#{col})\n"
            end
    end

    end
    File.open("resultados\\resultados-lexico.txt","w"){|f|
        f.write(analisis)
      }
    intAnalisis += "EOF|EOF|EOF#"
    File.open("resultados\\intern-lex.txt","w"){|f|
        f.write(intAnalisis)
    }
    
    File.open("resultados\\errores-lexico.txt","w"){|f|
    f.write(errores)
    }
    puts analisis
end

main