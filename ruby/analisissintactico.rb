# segundo analisis sintactico desarrollado en base a Tiny
# porque si
# c:
# Autor: Cesar Reyes

require 'rubygems'
require 'pp'
require 'json'
require_relative 'tree'
require_relative 'util'

# Arbol Original xdd
# class Tree 
#     def initialize
#         @a = 0
#         @b = ""
#         @data = []
#         @h = []
#         @linea = 0
#     end
#     def hermano
#         @h
#     end
#     def children
#         @data
#     end
#     def id
#         @a
#     end
#     def name
#         @b
#     end
#     def linea
#         @linea
#     end
#     def addchildren(value)
#         @data.push(value)
#     end
#     def addhermano(value)
#         @h.push(value)
#     end
#     def setid(value)
#         @a = value
#     end
#     def setname(value)
#         @b = value
#     end
#     def setlinea(ln)
#         @linea = ln
#     end
#     def getnombre
#         @b
#     end
#     def to_json(options = {})
#         #JSON.pretty_generate(self,options)
#          {'id' => @a, 'name' => @b, 'children' => @data}.to_json
#     end
#     # attr_accessor :name, :id, :children
# end

class Terminales
    def self.tipo
        ["integer","float","bool"]
    end
    def self.relacion
        ["<=","<",">",">=","==","!="]
    end
    def self.sumaopers
        ["+","-","++","--"]
    end
    def self.multopers
        ["*","/","%"]
    end
end

class Clasep
    def main
        @archivo = ARGV[0] #Recibe el nombre del archivo
        @texto = leer_archivo(@archivo) #abre el archivo y regresa texto plano
        @lineas = @texto.split("#")# divide el texto por lineas delimitadas por '#'
        @tokens = []
        @contador = 0
        @errores = ""
        @tkn = []
        @util = Util.new
        @id = 1
        @preverror = ""
        #divide cada linea por token y descripcion
        @lineas.each do |exp|
            @tokens << exp.split("|")
        end
        pp @tokens
        programa
    end    
    def leer_archivo(nombre)
        #abre un archivo y regresa su contenido
        archivo = File.open(nombre)
        texto = archivo.read
        archivo.close
        return texto
    end
end

def errorsintaxis(mensaje)
    @errores += "Error de sintaxis en #{@tkn[2]}"
end

def matchdes(esperado)
    if esperado == @tkn[1]
        gettoken
        @preverror = ""
    else
        if @preverror == "#{@tkn[0]},#{esperado}"
            gettoken
            @preverror = ""
            return
        end
        aux = @tokens[@contador-2]
        @errores += "Error en " + aux[2] + ", esperado: " + esperado + " leido: #{@tkn[0]}\n"
        @preverror = "#{@tkn[0]},#{esperado}"
    end
end

def match(esperado)
    if esperado == @tkn[0]
        gettoken
        @preverror = ""
    else
        if @preverror == "#{@tkn[0]},#{esperado}"
            gettoken
            @preverror = ""
            return
        end
        aux = @tokens[@contador-2]
        @errores += "Error en " + aux[2] + ", esperado: " + esperado + " leido: #{@tkn[0]}\n"
        @preverror = "#{@tkn[0]},#{esperado}"
    end
end

def listaSentencias
    t = sentencia
    p = t
    unless t.nil?
        if t.getnombre == ":="
            match(";")
        end
    end

    while @tkn[0] != "EOF" && @tkn[0] != "}" && @tkn[0] != "else" && @tkn[0] != "until"
        unless t.nil?
            unless t.getnombre == ":="
                unless t.getnombre == "if"
                    match(";")
                else
                    # gettoken
                    # match("}")
                    if @tkn[0]==";"
                        match(";")
                    end
                end
            end
        end
        q = sentencia
        unless q.nil?
            if t.nil?
               t = p = q
            else
                t.addhermano(q)
                if q.hermano.size>0
                    i = 0
                    while i<q.hermano.size
                    t.addhermano(q.hermano[i])
                    i+=1
                    end
                end
                
                p = q 
            end
        end
        hh = t.hermano
        lidx = hh.length-1
        hh = hh[lidx]
        if t.nil? 
            match(";")
        elsif t.getnombre == ":=" && hh.getnombre != "while" && hh.getnombre != "if" && hh.getnombre != "else" && hh.getnombre != "until"
            match(";")
        end
    end
    return t
end 

def sentencia
    t = nil
    lola = @tkn[0]
    case @tkn[0]
    when "if"
        t = seleccion
    when "while"
        t = iteracion
    when "do"
        t = repeticion
    when "read"
        t = sentread
    when "write"
        t = sentwrite
    when "{"
        t = bloque
    else
        if @tkn[1] == "identificador"
            t = asignacion
        elsif @tkn[0] != "}"
            @errores += "Error en " + @tkn[2] + " no se reconoce token: " + @tkn[0] + "\n"
        end
    end
    return t
end

def seleccion
    aux = []
    linea = @tkn[2]
    lnw = linea[1..-1].split(",")
    linea = lnw[0]
    t = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
    # t.setid(@id)
    @id += 1
    # t.setname(@tkn[0])
    match("if")
    match("(")
    t.addchildren(expresion)
    match(")")
    aux << t
    if @tkn[0] == "then"
        t = nil
        linea = @tkn[2]
        lnw = linea[1..-1].split(",")
        linea = lnw[0]
        t = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
        # t.setid(@id)
        @id += 1
        # t.setname(@tkn[0])
        aux << t
    end
    match("then")
    x = bloque
    unless x.nil?
        t.addchildren(x)
        x.hermano.each{|z|
            t.addchildren(z)
        }
    end
    
    if @tkn[0] == "else"
        t = nil
        linea = @tkn[2]
        lnw = linea[1..-1].split(",")
        linea = lnw[0]
        t = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
        # t.setid(@id)
        @id += 1
        # t.setname(@tkn[0])
        match("else")
        x = bloque
        unless x.nil?
            t.addchildren(x)
            x.hermano.each{|z|
                t.addchildren(z)
            }
        end
        aux << t
    end
    if aux.size > 0
        t = aux[0]
        i=1
        while i<aux.size
            t.addhermano(aux[i])
            i+=1
        end
    end
    return t
end

def iteracion
    linea = @tkn[2]
    lnw = linea[1..-1].split(",")
    linea = lnw[0]
    t = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
    # t.setid(@id)
    @id+=1
    # t.setname("while")
    match("while")
    match("(")
    t.addchildren(expresion)
    match(")")
    x = bloque
    unless x.nil?
        t.addchildren(x)
        x.hermano.each{|z|
            t.addchildren(z)
        }
    end
    return t
end

def repeticion
    linea = @tkn[2]
    lnw = linea[1..-1].split(",")
    linea = lnw[0]
    t = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
    # t.setid(@id)
    @id += 1
    # t.setname(@tkn[0])
    match("do")
    x = bloque
    unless x.nil?
        t.addchildren(x)
        x.hermano.each{|z|
            t.addchildren(z)
        }
    end
    linea = @tkn[2]
    lnw = linea[1..-1].split(",")
    linea = lnw[0]
    t2 = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
    # t2.setid(@id)
    @id += 1
    # t2.setname(@tkn[0])
    match("until")
    match("(")
    t2.addchildren(expresion)
    match(")")
    match(";")
    t.addhermano(t2)
    return t
end

def asignacion
    t = Tree.new
    # t.setid(@id)
    # @id += 1
    aux = @tkn
    matchdes("identificador")
    if @tkn[0] == "++"
        @tokens.insert(@contador+1,[":=","asignacion", @tokens[@contador][2]])
        @tokens.insert(@contador+2,[aux[0],"identificador", @tokens[@contador][2]])
        @tokens.insert(@contador+3,["+","suma", @tokens[@contador][2]])
        @tokens.insert(@contador+4,["1","entero", @tokens[@contador][2]])
        @tokens.insert(@contador+5,[";","separacion", @tokens[@contador][2]])
        @tokens.delete_at(@contador)
        gettoken
    elsif @tkn[0] == "--"
        @tokens.insert(@contador+1,[":=","asignacion", @tokens[@contador][2]])
        @tokens.insert(@contador+2,[aux[0],"identificador", @tokens[@contador][2]])
        @tokens.insert(@contador+3,["-","resta", @tokens[@contador][2]])
        @tokens.insert(@contador+4,["1","entero", @tokens[@contador][2]])
        @tokens.insert(@contador+5,[";","separacion", @tokens[@contador][2]])
        @tokens.delete_at(@contador)
        gettoken
    end
    if @tkn[0] == ":="
        linea = @tkn[2]
        lnw = linea[1..-1].split(",")
        linea = lnw[0]
        t = @util.newStmtNode(@tkn[1],@id,":=",linea)
        # t.setname(":=")
        match(":=")
        @id +=1
        t2 = @util.newStmtNode(aux[1],@id,aux[0],linea)
        # t2.setid(@id)
        # t2.setname(aux)
        @id += 1
        t.addchildren(t2)
        t.addchildren(expresion)
    else
        lin1 = @tkn[2].split(",")
        lin1 = lin1[0].tr("(","")
        lin2 = lin1
        aux = @tokens[@contador-2]
        while (lin1 == lin2 && @tkn[0] != ";" && @tkn[0] != "EOF")
            @errores += "Error en " + aux[2] + ", esperado: ; leido: #{@tkn[0]}\n"
            gettoken
            lin2 = @tkn[2].split(",")
            lin2 = lin2[0].tr("(","")
        end
        return nil
    end
    return t
end

def sentread
    linea = @tkn[2]
    lnw = linea[1..-1].split(",")
    linea = lnw[0]
    t = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
    # t.setid(@id)
    @id += 1
    # t.setname(@tkn[0])
    match("read")
    if @tkn[1]=="identificador"
        linea = @tkn[2]
        lnw = linea[1..-1].split(",")
        linea = lnw[0]
        t2 = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
        # t2.setid(@id)
        @id += 1
        # t2.setname(@tkn[0])
        matchdes("identificador")
        t.addchildren(t2)
    end
    if @tkn[0] != ";"
        lin1 = @tkn[2].split(",")
        lin1 = lin1[0].tr("(","")
        lin2 = lin1
        aux = @tokens[@contador]
        while (lin1 == lin2 && @tkn[0] != ";" && @tkn[0] != "EOF")
            @errores += "Error en " + aux[2] + ", esperado: ; leido: #{@tkn[0]}\n"
            gettoken
            lin2 = @tkn[2].split(",")
            lin2 = lin2[0].tr("(","")
        end
    # else
    #     match(";")
    end
    
    return t
end

def sentwrite
    linea = @tkn[2]
    lnw = linea[1..-1].split(",")
    linea = lnw[0]
    t = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
    # t.setid(@id)
    @id += 1
    # t.setname(@tkn[0])
    match("write")
    if @tkn[1] == "cadena"
        linea = @tkn[2]
        lnw = linea[1..-1].split(",")
        linea = lnw[0]
        t2 = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
        # t2.setid(@id)
        @id += 1
        # t2.setname(@tkn[0])
        t.addchildren(t2)
    end
    matchdes("cadena")
    if @tkn[0] == ","
        match(",")
        t.addchildren(expresion)
    end
    if @tkn[0] != ";"
        lin1 = @tkn[2].split(",")
        lin1 = lin1[0].tr("(","")
        lin2 = lin1
        aux = @tokens[@contador]
        while (lin1 == lin2 && @tkn[0] != ";" && @tkn[0] != "EOF")
            @errores += "Error en " + aux[2] + ", esperado: ; leido: #{@tkn[0]}\n"
            gettoken
            lin2 = @tkn[2].split(",")
            lin2 = lin2[0].tr("(","")
        end
    # else
    #     match(";")
    end
    return t
end

def expresion
    t = expresionsimple
    if Terminales.relacion.include? @tkn[0]
        linea = @tkn[2]
        lnw = linea[1..-1].split(",")
        linea = lnw[0]
        p = @util.newExpNode(@tkn[1],@id,@tkn[0],linea)
        # p = Tree.new
        # p.setid(@id)
        @id += 1
        # p.setname(@tkn[0])
        p.addchildren(t)
        t = p
        match(@tkn[0])
        t.addchildren(expresionsimple)
    end
    return t
end

def expresionsimple
    t = termino
    while Terminales.sumaopers.include? @tkn[0]
        linea = @tkn[2]
        lnw = linea[1..-1].split(",")
        linea = lnw[0]
        p = @util.newExpNode(@tkn[1],@id,@tkn[0],linea)
        # p = Tree.new
        # p.setid(@id)
        @id += 1
        # p.setname(@tkn[0])
        p.addchildren(t)
        t = p
        match(@tkn[0])
        t.addchildren(termino)
    end
    return t
end

def termino
    t = factor
    while Terminales.multopers.include? @tkn[0]
        linea = @tkn[2]
        lnw = linea[1..-1].split(",")
        linea = lnw[0]
        p = @util.newExpNode(@tkn[1],@id,@tkn[0],linea)
        # p = Tree.new
        # p.setid(@id)
        @id += 1
        # p.setname(@tkn[0])
        p.addchildren(t)
        t = p
        match(@tkn[0])
        t.addchildren(factor)
    end
    return t
end

def factor
    t = nil
    if @tkn[1] == "entero" ||@tkn[1] == "decimal"
        linea = @tkn[2]
        lnw = linea[1..-1].split(",")
        linea = lnw[0]
        t = @util.newExpNode(@tkn[1],@id,@tkn[0],linea)
        # t = Tree.new
        # t.setid(@id)
        @id += 1
        # t.setname(@tkn[0])
        matchdes(@tkn[1])
    elsif @tkn[1] == "identificador"
        linea = @tkn[2]
        lnw = linea[1..-1].split(",")
        linea = lnw[0]
        t = @util.newExpNode(@tkn[1],@id,@tkn[0],linea)
        # t = Tree.new
        # t.setid(@id)
        @id += 1
        # t.setname(@tkn[0])
        matchdes("identificador")
        
    elsif @tkn[0] == "("
        t = Tree.new
        match("(")
        t = expresion
        match(")")
    else
        @errores += "Error en " + @tkn[2] + " no se reconoce token: " + @tkn[0] + "\n"
    end
    return t
end

def listaDeclaracion
    aux = []
    while Terminales.tipo.include? @tkn[0]
        linea = @tkn[2]
        lnw = linea[1..-1].split(",")
        linea = lnw[0]
        x = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
        # x.setid(@id)
        @id+=1
        # lola = @tkn[0]
        # x.setname(@tkn[0])
        match(@tkn[0])        
        y = listavariables
        unless y.nil?
            x.addchildren(y)
            y.hermano.each{|z|
                x.addchildren(z)
            }
        end
        match(";")
        aux << x
    end
    if aux.size > 0
        t = aux[0]
        i=1
        while i<aux.size
            t.addhermano(aux[i])
            i+=1
        end
    end
    return t
end

def listavariables
    linea = @tkn[2]
    lnw = linea[1..-1].split(",")
    linea = lnw[0]
    t = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
    # t.setid(@id)
    @id+=1
    # t.setname(@tkn[0])
    matchdes("identificador")
    while @tkn[0] == ","
        match(",")
        if @tkn[1]=="identificador"
            linea = @tkn[2]
            lnw = linea[1..-1].split(",")
            linea = lnw[0]
            t2 = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
            # t2.setid(@id)
            @id+=1
            # t2.setname(@tkn[0])
            t.addhermano(t2)
        end
        matchdes("identificador")
    end
    return t
end

def bloque
    t = Tree.new
    match("{")
    t = listaSentencias
    match("}")
    return t
end

def gettoken
    @tkn = @tokens[@contador]
    @contador += 1
end

def programa
    gettoken
    t = Tree.new
    y= Tree.new
    lola = @tkn[0] 
    puts lola
    if @tkn[0] == "main"
        linea = @tkn[2]
        lnw = linea[1..-1].split(",")
        linea = lnw[0]
        t = @util.newStmtNode(@tkn[1],@id,@tkn[0],linea)
        # t.setid(@id)
        @id += 1
        # t.setname("main")
    end
    match("main")
    match("{")
    # t.addchildren(listaDeclaracion)
    y = listaDeclaracion
    unless y.nil?
        t.addchildren(y)
        y.hermano.each{|z|
            t.addchildren(z)
        }
    end
    x = listaSentencias
    unless x.nil?
        t.addchildren(x)
        x.hermano.each{|z|
            t.addchildren(z)
        }
    end
    
    
    match("}")
    carpeta = Dir.pwd
    pp t
    jsonn = t.to_json
    File.open(carpeta + "/resultados/arbolsintactico.txt","w"){|f|
        f.write("[" + jsonn + "]")
    }

    File.open(carpeta + "/resultados/intern-sint.txt","w"){|f|
        f.write("[" + jsonn + "]")
    }

   File.open(carpeta + "/resultados/erroressintactico.txt","w"){|f|
       f.write(@errores)
   }
end




a = Clasep.new
a.main