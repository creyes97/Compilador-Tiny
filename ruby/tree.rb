class Tree 
    def initialize
        @a = 0
        @b = ""
        @data = []
        @h = []
        @linea = 0
        @tipoNodo = 0 # 2 Statement y exp
        @tipo = ""  # Para los tipos sean if, repeat, operador, constante etc
        @attr = [] # 3 atributos: op, val, nombre...
        @expType = "" # de 0 a 3
        @attr<< 0
        @attr<< -1
        @attr<< -1
    end
    def hermano
        @h
    end
    def children
        @data
    end
    def id
        @a
    end
    def name
        @b
    end
    def linea
        @linea
    end
    def tipoNodo
        @tipoNodo
    end
    def tipo
        @tipo
    end
    def expType
        @expType
    end
    def attrib
        @attr
    end
    def hasValue
        @attr[0] = 1
    end
    def addchildren(value)
        @data.push(value)
    end
    def modChildren(idx,value)
        @data[idx] = value
    end
    def addhermano(value)
        @h.push(value)
    end
    def setid(value)
        @a = value
    end
    def setname(value)
        @b = value
    end
    def setlinea(ln)
        @linea = ln
    end
    def setTipoNodo(value)
        @tipoNodo = value
    end
    def setTipo(value)
        @tipo = value
    end
    def setexpType(value)
        @expType = value
    end
    def setValue(value)
        @attr[1] = value
    end
    def getnombre
        @b
    end
    def to_json(options = {})
        #JSON.pretty_generate(self,options)
        # {'id' => @a, 'name' => @b, 'children' => @data}.to_json
        {'id' => @a, 'name' => @b, 'line'=>@linea, 'NodeKind'=>@tipoNodo, 'kind'=>@tipo, 'Attr'=>@attr,'Exptype'=>@expType, 'children' => @data}.to_json
    end
    def to_json_complete
        {'id' => @a, 'name' => @b, 'sibling' => @h, 'line'=>@linea, 'NodeKind'=>@tipoNodo, 'kind'=>@tipo, 'Attr'=>@attr,'Exptype'=>@expType, 'children' => @data}.to_json
    end
    def []=(p1,p2)
        case p1
        when "id"
            @a = p2
        when "name"
            @b = p2
        when "children"
            @data = p2
        when "sibling"
            @h = p2
        when "line"
            @linea = p2
        when "NodeKind"
            @tipoNodo = p2
        when "kind"
            @tipo = p2
        when 'Attr'
            @attr = p2
        when 'Exptype'
            @expType = p2
        end
    end
    # attr_accessor :name, :id, :children
end