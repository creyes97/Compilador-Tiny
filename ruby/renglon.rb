require 'rubygems'

require 'json'

class Renglon
    attr_reader :Lineas, :Nombre, :Memloc, :Valor

    def initialize(lineas, nombre, memloc)
        @Lineas = lineas
        @Nombre = nombre
        @Memloc = memloc
        @Valor = 0
    end

    def Lineas=(value)
        @Lineas = value
    end

    def Valor=(value)
        @Valor = value
    end

    def to_json(options = {})
        {'nombre' => @Nombre, 'Memoria'=>@Memloc, 'lineas'=>@Lineas, 'valor'=>@Valor}.to_json
    end
end