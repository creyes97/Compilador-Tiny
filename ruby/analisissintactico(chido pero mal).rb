require 'rubygems'
require 'pp'
require 'json'

# class TreeRepresenter < Representable::Decorator
#     include Representable::JSON
    
#     property :id
#     property :name
#     collection :children

# end

# class JSONable
#     def to_json
#         hash = {}
#         self.instance_variables.each do |var|
#             hash[var] = self.instance_variable_get var
#         end
#         hash.to_json
#     end
#     def from_json! string
#         JSON.load(string).each do |var, val|
#             self.instance_variable_set var, val
#         end
#     end
# end

class Tree 
    def initialize
        @a = 0
        @b = ""
        @data = []
    end
    def children
        @data
    end
    def id
        @a
    end
    def name
        @b
    end
    def addchildren(value)
        @data.push(value)
    end
    def setid(value)
        @a = value
    end
    def setname(value)
        @b = value
    end
    def to_json(options = {})
        #JSON.pretty_generate(self,options)
         {'id' => @a, 'name' => @b, 'children' => @data}.to_json
    end
    # attr_accessor :name, :id, :children
end

# class Tree
#     def to_json
#         {}.to_json
#     end
    
#     def self.from_json
# end



class Terminales
    def self.tipo
        ["integer","float","bool"]
    end
    def self.relacion
        ["<=","<",">",">=","==","!="]
    end
    def self.sumaopers
        ["+","-","++","--"]
    end
    def self.multopers
        ["*","/","%"]
    end
end

class Clasep
    def main
        @archivo = ARGV[0] #Recibe el nombre del archivo
        @texto = leer_archivo(@archivo) #abre el archivo y regresa texto plano
        @lineas = @texto.split("#")# divide el texto por lineas delimitadas por '#'
        @tokens = []
        @contador = 0
        @errores = ""
        @niveltoken = 0
        @arbol = ""
        @parcial = ""
        @espera = []
        @esperaid = []
        @esasigna = false
        
        #divide cada linea por token y descripcion
        @lineas.each do |exp|
            @tokens << exp.split("|")
        end
        # pp @tokens
        programa
    end    
    def leer_archivo(nombre)
        #abre un archivo y regresa su contenido
        archivo = File.open(nombre)
        texto = archivo.read
        archivo.close
        return texto
    end
end

def count_em(string, substring)
    string.scan(/(?=#{substring})/).count
end

def programa
    #se define el inicio de la gramatica EBNF para revisar
    matchtok("main")
    @niveltoken += 1
    matchtok("{")
    
    listadeclaracion
    listasentencias
    
    matchtok("}")
    @niveltoken -=1
    tree = generaArbol(0)
    puts @arbol
    File.open("arbolsintactico.txt","w"){|f|
         f.write("[" + tree.to_json + "]")
    }

    File.open("erroressintactico.txt","w"){|f|
        f.write(@errores)
    }
end

def generaArbol(inicio)
    aux = Tree.new
    nodos = @arbol.split("\n")
    aux.setid(inicio)
    nom = nodos[inicio].tr('|','')
    aux.setname(nom)
    en = nodos[inicio].split("|")
    nivela = en.size - 1 #count_em(en, "\|")
    i = inicio+1

    while(i<nodos.size)
        en = nodos[i].split("|")
        nivelb = en.size-1
        diferencia = nivelb-nivela
        if diferencia == 1
            hijo = generaArbol(i)
            aux.addchildren(hijo)
        elsif diferencia == 0
            return aux
        end
        i+=1
    end
    return aux
end

def listadeclaracion
    if declaracion
        matchtok(";")
        listadeclaracion
        return true
    end
    return false
end
def declaracion
    Terminales.tipo.each{|f|
        token = @tokens[@contador][0]
        if f == token
            matchtok(f)
            @niveltoken += 1
            listavariables
            @niveltoken -=1
            return true
        end
    }
    return false
end
def listavariables
    tok = @tokens[@contador][0]
    matchdes("identificador","")
    if @tokens[@contador][0] == ","
        @contador += 1
        listavariables
    end
end
def listasentencias
    if seleccion || iteracion || repeticion || sentread || sentwrite || bloque || asignacion
        listasentencias
    elsif listadeclaracion
        listasentencias
    end
end

def seleccion
    # puts @contador
    # pp @tokens
    
    if @tokens[@contador][0] == "if"
        matchtok("if")
        matchtok("(")
        
        expresion
        @niveltoken += 1
            vaciarpendientes
        @niveltoken -= 1
        matchtok(")")
        matchtok("then")
        if !bloque
            @niveltoken +=1
            @errores += "Error en " + @tokens[@contador-1][2] + " se esperaba un {, token siguiente: " + @tokens[@contador][0] + "\n"
            listasentencias
            @niveltoken -=1
            matchtok("}")
        end
        if @tokens[@contador][0] == "else"
            matchtok("else")
            if !bloque
                @niveltoken +=1
                @errores += "Error en " + @tokens[@contador-1][2] + " se esperaba un {, token siguiente: " + @tokens[@contador][0] + "\n"
                listasentencias
                @niveltoken -=1
                matchtok("}")
            end
        end
        return true
    end
    return false
end

def iteracion
    if @tokens[@contador][0] == "while"
        matchtok("while")
        matchtok("(")
        
        esperaid = Array.new()
        espera = Array.new()
        expresion
        @niveltoken+=1
        vaciarpendientes
        
        @niveltoken-=1
        matchtok(")")
        if !bloque
            @niveltoken +=1
            @errores += "Error en " + @tokens[@contador-1][2] + " se esperaba un {, token siguiente: " + @tokens[@contador][0] + "\n"
            listasentencias
            @niveltoken -=1
            matchtok("}")
        end
        return true
    end
    return false
end 

def repeticion
    if @tokens[@contador][0] == "do"
        matchtok("do")
        @niveltoken +=1
        if !bloque
            @niveltoken +=1
            @errores += "Error en " + @tokens[@contador-1][2] + " se esperaba un {, token siguiente: " + @tokens[@contador][0] + "\n"
            listasentencias
            @niveltoken -=1
            matchtok("}")
        end
        @niveltoken -=1
        esperaid = Array.new
        espera = Array.new
        matchtok("until")
        matchtok("(")
        expresion
        @niveltoken+=1
        vaciarpendientes
        @niveltoken-=1
        matchtok(")")
        matchtok(";")
        return true
    end
    return false
end
def sentread
    if @tokens[@contador][0] == "read"
        matchtok("read")
        @niveltoken +=1
        matchdes("identificador","")
        @niveltoken -=1
        errorf = @contador-1
        while @tokens[@contador][0] != ";" && @tokens[@contador][1] != "identificador" && @tokens[@contador][1] != "reservada" && @tokens[@contador][1] != "llave cierra"
            @errores += "Error en " + @tokens[errorf][2] + " se esperaba un ;, token siguiente: " + @tokens[@contador][0] + "\n"
            @contador += 1
        end

        matchtok(";")
        return true
    end
    return false
end

def sentwrite
    if @tokens[@contador][0] == "write"
        matchtok("write")
        @niveltoken +=1
        matchdes("cadena","")
        if @tokens[@contador][0] == ","
            matchtok(",")
            @niveltoken -= 1
            expresion
            if @tokens[@contador][1] == "entero" || @tokens[@contador][1] == "decimal"
                expresion
            end
        else
            @niveltoken -=1
        end
        tkn = @tokens[@contador][0]
        errorf = @contador-1
        ren = @tokens[@contador][2].split(",")[0]
        ren = ren[1]
        ren2 = ren
        while @tokens[@contador][0] != ";" && ren == ren2
            @errores += "Error en " + @tokens[errorf][2] + " se esperaba un ;, token siguiente: " + @tokens[@contador][0] + "\n"
            @contador += 1
            ren2 = @tokens[@contador][2].split(",")[0]
            ren2 = ren2[1]
        end
        
        matchtok(";")
        return true
    end
    return false
end

def expresion
    @esperaid = Array.new
    expresionsimple
    @niveltoken += 1
    if relacion
        expresionsimple
        # vaciarid
    end
    validador = @arbol.split("\n")
    i = 0
    while i<validador.size
        validador[i] = validador[i].tr('|','')
        validador[i] = validador[i].tr("\n",'')
        i+=1
    end
    buscar = validador.last
    i=0
    tipo = ""
    while i<@tokens.size
        if @tokens[i][0] == buscar
            tipo = @tokens[i][1]
            break
        end
        i+=1
    end
    if tipo != "identificador"
        vaciarid
        # esperaid = Array.new
    end
    
    
    # vaciarpendientes
    @arbol += @parcial
    @niveltoken -= 1
end

def expresionsimple
    termino
    @niveltoken += 1
    if multop
        expresionsimple
        if @espera.length == 1
            @espera.each{|q|
                i=0
                while(i<@niveltoken+1)
                    @arbol += "|";
                    i += 1
                end
                @arbol += q + "\n"
                q = ""
            }
            @espera = Array.new
        end
        # vaciarid
    end
    
    @niveltoken -= 1
    
    
end

def termino
    factor
    @niveltoken += 1
    if sumaop
        termino
        if @espera.length == 1
            @espera.each{|q|
                i=0
                while(i<@niveltoken+1)
                    @arbol += "|";
                    i += 1
                end
                @arbol += q + "\n"
                q = ""
            }
            @espera = []
        end
    end
    @niveltoken -= 1
end

def relacion
    Terminales.relacion.each{|t|
        if t == @tokens[@contador][0]
            i=0
            while(i<@niveltoken)
                @arbol += "|";
                i += 1
            end
            @arbol += t + "\n"
            vaciarpendientes
            @contador+=1
            return true
        end
    }
    return false
end

def sumaop
    Terminales.sumaopers.each{|t|
        if t == @tokens[@contador][0]
            i=0
            while(i<@niveltoken)
                @arbol += "|";
                i += 1
            end
            @arbol += t + "\n"
            vaciarpendientes
            @contador+=1
            return true
        end
    }
    return false
end

def multop
    Terminales.multopers.each{|t|
        if t==@tokens[@contador][0]
            i=0
            while(i<@niveltoken)
                @arbol += "|";
                i += 1
            end
            @arbol += t + "\n"
            @arbol += @parcial
            @parcial = ""
            vaciarpendientes
            @contador+=1
            return true
        end
    }
    return false
end

def factor
    fff = @tokens[@contador][0]
    if @tokens[@contador][0] == "("
        matchtok("(")
        @niveltoken += 1
        expresion2
        @niveltoken -= 1
        matchtok(")")
    elsif @tokens[@contador][1] == "identificador"
        matchdes("identificador","espera")
    else
        revisarnum
    end
end

def revisarnum
    if @tokens[@contador][1] == "entero"
        matchdes("entero","espera")
    elsif @tokens[@contador][1] == "decimal"
        matchdes("decimal","espera")
    else
        @errores += "Error en " + @tokens[@contador-1][2] + " se esperaba un numero, token siguiente: " + @tokens[@contador][0] + "\n"
    end
end

def bloque
    if @tokens[@contador][0] == "{"
        matchtok("{")
        # @niveltoken +=1
        listasentencias
        while declaracion
            @errores += "Error en " + @tokens[@contador][2] + " se esperaba una sentencia, se leyó una declaracion\n"
            @contador += 1
            if @tokens[@contador][0] != "}"
                listasentencias
            end
        end
        # @niveltoken -=1
        matchtok("}")
        return true
    end
    return false
end

def vaciarpendientes
    if @espera != []
        @espera.each{|q|
            i=0
            while(i<@niveltoken+1)
                @arbol += "|";
                i += 1
            end
            @arbol += q + "\n"
            q = ""
        }
        @espera = Array.new
    end
end

def vaciarid
    if @esperaid != []
        @esperaid.each{|q|
            i=0
            while(i<@niveltoken+1)
                @arbol += "|";
                i += 1
            end
            @arbol += q + "\n"
            q = ""
        }
        @esperaid = Array.new
    end
end

def asignacion
    if @tokens[@contador][1] == "identificador" 
        matchdes("identificador", "asignacion")
        
        if @tokens[@contador][0] == "++"
            @tokens.insert(@contador+1,[":=","asignacion", @tokens[@contador][2]])
            @tokens.insert(@contador+2,["a","identificador", @tokens[@contador][2]])
            @tokens.insert(@contador+3,["+","suma", @tokens[@contador][2]])
            @tokens.insert(@contador+4,["1","entero", @tokens[@contador][2]])
            @tokens.delete_at(@contador)
        elsif @tokens[@contador][0] == "--"
            @tokens.insert(@contador+1,[":=","asignacion", @tokens[@contador][2]])
            @tokens.insert(@contador+2,["a","identificador", @tokens[@contador][2]])
            @tokens.insert(@contador+3,["-","resta", @tokens[@contador][2]])
            @tokens.insert(@contador+4,["1","entero", @tokens[@contador][2]])
            @tokens.delete_at(@contador)
        end
        errorf = @contador-1
        ren = @tokens[@contador][2].split(",")[0]
        ren = ren[1]
        ren2 = ren
        while @tokens[@contador][0] != ":=" && ren == ren2 && @tokens[@contador][0]!=";"
            @errores += "Error en " + @tokens[errorf][2] + " se esperaba un :=, token siguiente: " + @tokens[@contador][0] + "\n"
            @contador += 1
            ren2 = @tokens[@contador][2].split(",")[0]
            ren2 = ren2[1]
        end
        if @tokens[@contador][0] == ":="
            matchdes("asignacion", "")
            vaciarid
            expresion
            vaciarpendientes
            vaciarid
        elsif @tokens[@contador][0] == ";"
            esperaid = Array.new()
            espera = Array.new()
        end
        matchtok(";")
        return true
    end
    return false
end

def matchtok(esperado)
    token = @tokens[@contador][0]
    tokend = @tokens[@contador][1]
    if esperado != token
        @errores += "Error en " + @tokens[@contador-1][2] + " se esperaba un " + esperado + ", token siguiente: " + @tokens[@contador][0] + "\n"
        # if(esperado == ";")
        #    @contador += 1
        #    matchtok(";") 
        # end
        return
    end
    if tokend != "llave cierra" && tokend != "llave abre" && tokend !="separacion" && tokend != "parentesis abre"&& tokend != "parentesis cierra"
        i=0
        while(i<@niveltoken)
            @arbol += "|";
            i += 1
        end
        @arbol += token + "\n"
    end
    @contador += 1
    
end

def matchdes(esperado, fuente)
    tokend = @tokens[@contador][1]
    token = @tokens[@contador][0]
    tokensi = @tokens[@contador+1][0]
    if esperado != tokend
        @errores += "Error en " + @tokens[@contador][2]+ " se esperaba un "+ esperado + ", token siguiente: " + @tokens[@contador][0] +"\n"
        return
    end

    if (fuente == "asignacion" || fuente=="espera") && esperado != "identificador" 
        @espera << token 
        @contador += 1
        return
    elsif fuente == "asignacion" || fuente=="espera" 
        @esperaid << token
        @contador += 1
        return
    end

    if tokend != "llave cierra" && tokend != "llave abre" && tokend !="separacion" && tokend != "parentesis abre"&& tokend != "parentesis cierra"
        i=0
        while(i<@niveltoken)
            @arbol += "|";
            i += 1
        end
        @arbol += token + "\n"
        vaciarpendientes
    end
    @contador += 1
end



######################
def expresion2
    expresionsimple2
    @niveltoken += 1
    if relacion2
        expresionsimple2
    end
    vaciarid2
    @niveltoken -= 1
end

def expresionsimple2
    termino2
    @niveltoken += 1
    if multop2
        expresionsimple2
        if @espera.length == 1
            @espera.each{|q|
                i=0
                while(i<@niveltoken+1)
                    @parcial += "|";
                    i += 1
                end
                @parcial += q + "\n"
                q = ""
            }
            @espera = []
        end
        # vaciarid
    end
    
    @niveltoken -= 1
    
    
end

def termino2
    factor
    #@niveltoken += 1
    if sumaop2
        termino2
        if @espera.length == 1
            @espera.each{|q|
                i=0
                while(i<@niveltoken+1)
                    @parcial += "|";
                    i += 1
                end
                @parcial += q + "\n"
                q = ""
            }
            @espera = []
        end
    end
    #@niveltoken -= 1
end

def relacion2
    Terminales.relacion.each{|t|
        if t == @tokens[@contador][0]
            i=0
            while(i<@niveltoken)
                @parcial += "|";
                i += 1
            end
            @parcial += t + "\n"
            vaciarpendientes2
            @contador+=1
            return true
        end
    }
    return false
end

def sumaop2
    Terminales.sumaopers.each{|t|
        if t == @tokens[@contador][0]
            i=0
            while(i<@niveltoken)
                @parcial += "|";
                i += 1
            end
            @parcial += t + "\n"
            vaciarpendientes2
            @contador+=1
            return true
        end
    }
    return false
end

def multop2
    Terminales.multopers.each{|t|
        if t==@tokens[@contador][0]
            i=0
            while(i<@niveltoken)
                @parcial += "|";
                i += 1
            end
            @parcial += t + "\n"
            vaciarpendientes2
            @contador+=1
            return true
        end
    }
    return false
end

def vaciarpendientes2
    if @espera != []
        @espera.each{|q|
            i=0
            while(i<@niveltoken+1)
                @parcial += "|";
                i += 1
            end
            @parcial += q + "\n"
            q = ""
        }
        @espera = []
    end
end

def vaciarid2
    if @esperaid != []
        @esperaid.each{|q|
            i=0
            while(i<@niveltoken+1)
                @parcial += "|";
                i += 1
            end
            @parcial += q + "\n"
            q = ""
        }
        @esperaid = []
    end
end



a = Clasep.new
a.main


