require_relative 'tree'
class Util
    def newStmtNode(kind,id,name,linea)
        t = Tree.new
        t.setid(id)
        t.setname(name)
        t.setlinea(linea)
        t.setTipoNodo(0)
        t.setTipo(kind)
        return t
    end

    def newExpNode(kind,id,name,linea)
        t = Tree.new
        t.setid(id)
        t.setname(name)
        t.setlinea(linea)
        t.setTipoNodo(1)
        t.setTipo(kind)
        return t
    end

    def recorrerTipos(t)
        unless t.nil?
            n = t.name
            tt = t.expType
            at = t.attrib
            unless at[0] == 0
                nName = "#{n}   :#{tt} (#{at[1]})"
            else
                nName = "#{n}   :#{tt}"
            end
            
            t.setname(nName)
            i=0
            al = t.children.length
            while i < al
                childs = t.children
                f = childs[i]
                aux = recorrerTipos(f);
                t.modChildren(i,aux)
                i+=1
            end
        end
        return t
    end
end