require 'rubygems'

require 'json'
require_relative 'renglon'
class Tablasimbolos
    
    def initialize
        @Tabla = {}
    end
    def insertar(nombre, linea, lugar)
        tab = Renglon.new linea, nombre, lugar
        @Tabla[nombre] = tab
    end
    def buscar(nombre)
        a = @Tabla[nombre]
        if a.nil?
            return -1
        else
            return 1
        end
    end 
    def modificar(nombre, linea, lugar)
        if buscar(nombre) == 1
            existente = @Tabla[nombre]
            existente.Lineas = existente.Lineas + ", " + linea
            @Tabla[nombre] = existente
        end
    end
    def modificarV(nombre,valor)
        if buscar(nombre) == 1
            existente = @Tabla[nombre]
            existente.Valor = valor
            @Tabla[nombre] = existente
        end
    end
    def json(options = {})
        renglones = []
        @Tabla.each {|r|
            renglones<<r[1]
        }
        a = renglones.to_json
    end
end