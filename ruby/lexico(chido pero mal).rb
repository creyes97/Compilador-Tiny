class Convenciones
  def self.reservadas
    ["main", "if", "then", "else", "end", "do", "while", "repeat", "until", "read", "write", "float", "integer", "bool"]
  end
  def self.especiales
    ["+", "-", "*", "/", "%", "<", "<=", ">", ">=", "==", "!=", ":=", "(", ")", "{", "}", "//", "/*", "++", "--"]
  end
  def self.letras
    "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
  end
  def self.numeros
    "1234567890"
  end
end

class Banderas
  def self.suma
    1
  end
  def self.resta
    2
  end
  def self.comparacionmen
    3
  end
  def self.comparacionmay
    31
  end
  def self.igual
    4
  end
  def self.asigna
    41
  end
  def self.niega
    42
  end
  def self.modulo
    5
  end
  def self.division
    6
  end
  def self.comentariolin
    7
  end
  def self.comentariomulti
    8
  end
  def self.cierremulti
    9
  end
  def self.cadena
    10
  end
end



def leer_archivo(nombre)
  archivo = File.open(nombre)
  texto = archivo.read
  archivo.close
  return texto
end

def buscar_reservada(palabra)
  Convenciones.reservadas.each {|a|
    if (palabra == a)
      return true
    end
  }
  return false
end

def buscar_identi(palabra)
  contador = 0
  if(palabra.length<1)
    return false
  end

  while(contador<palabra.length)
    c = palabra[contador,1]
    begin
      if(contador ==1)
        if(!(Convenciones.letras.include? c))
          return false
        end
      end
      var1 = Convenciones.letras.include? c
      var2 = Convenciones.numeros.include? c

      unless var1 || var2
        return false
      end
    rescue
      return false
    end

    contador += 1
  end
  return true
end

def buscar_numero(palabra)
  contador =0
  while(contador<palabra.length)
    c = palabra[contador,1]
    if(!(Convenciones.numeros.include? c))
      return false
    end
    contador += 1
  end
  return true
end


def main
  archivo = ARGV[0]
  texto = leer_archivo(archivo)
  texto = texto.split ("\n")
  fila = 0
  analisiscompleto = ""#String que concatena los resultados completos del analisis
  bandera = 0

  #Columna y fila donde inician comentarios
  columnacoment = 0
  filacoment = 0
  #columna y fila donde inicia el string
  columnastr = 0
  filastr = 0

  texto.each {|e|
    columna = 0
    fila += 1
    analisislinea = ""
    #palabras = e.split(/(?<=[+*\/%<>=!:(){},])/)
    palabras = e.split(/([+\-*\/%<>=!:(){},";.\s])/)

    palabras.each{|f|
      actual = ""
      analisisgen=true
      if f==""
        next
      end
      case bandera
      when Banderas.cadena
        if f== "\""
          actual += "#{f} -> Cadena (#{filastr},#{columnastr}) "
          bandera = 0
          analisisgen = false
        else
          actual += "#{f}"
          analisisgen = false
        end
      when Banderas.suma
        if f=="+"
          actual += "++ -> Incremento (#{fila},#{(columna-1)}) "
          bandera = 0
          analisisgen = false
        else
          actual += "+ -> suma (#{fila},#{(columna-1)}) \n"
          bandera = 0
        end
      when Banderas.resta
        if f=="-"
          actual += "-- -> Decremento (#{fila},#{(columna-1)}) "
          bandera =0
          analisisgen = false
        else
          actual += "- -> resta (#{fila},#{(columna-1)}) \n"
          bandera = 0
        end
      when Banderas.comparacionmen
        if f=="="
          actual += "<= -> Menor o igual (#{fila},#{columna-1}) "
          bandera = 0
          analisisgen = false
        else
          actual += "< -> Menor que (#{fila},#{columna-1}) \n"
          bandera = 0
        end
      when Banderas.comparacionmay
        if f=="="
          actual += ">= -> Mayor o igual (#{fila},#{columna-1}) "
          bandera = 0
          analisisgen = false
        else
          actual += "> -> Mayor Que (#{fila},#{columna-1}) \n"
          bandera = 0
        end
      when Banderas.asigna
        if f=="="
          actual += ":= -> Asignacion (#{fila},#{columna-1}) "
          bandera = 0
          analisisgen = false
        else
          actual += ": -> Error (#{fila},#{columna-1}) \n"
          bandera = 0
        end
      when Banderas.igual
        if f=="="
          actual += "== -> Igual (#{fila},#{columna-1}) "
          bandera = 0
          analisisgen = false
        else
          actual += "= -> Error (#{fila},#{columna-1}) \n"
          bandera = 0
        end
      when Banderas.niega
        if f=="="
          actual += "!= -> Diferencia (#{fila},#{columna-1}) "
          bandera = 0
          analisisgen = false
        else
          actual += "! -> Negacion (#{fila},#{columna-1}) \n"
          bandera = 0
        end
      when Banderas.division
        if f=="/"
          actual += "//"
          bandera = Banderas.comentariolin
          columnacoment = columna-1
          analisisgen = false
        else
          if f=="*"
            actual += "/*"
            bandera = Banderas.comentariomulti
            columnacoment = columna-1
            filacoment = fila
            analisisgen = false
          else
            actual += "/ -> Division (#{fila},#{columna-1}) \n"
            bandera = 0
          end

        end
      when Banderas.comentariolin
        actual += "#{f}"
        analisisgen = false
      when Banderas.comentariomulti
        if f=="*"
          bandera = Banderas.cierremulti
        end
        actual += "#{f}"
        analisisgen=false
      when Banderas.cierremulti
        if f=="/"
          bandera = 0
          actual += "#{f} -> Comentario Multilinea (#{filacoment},#{columnacoment})"
        else
          bandera = Banderas.comentariomulti
          actual += "#{f}"
        end
        analisisgen = false
      else

      end
      if analisisgen
        if buscar_reservada(f)
          actual += "#{f} -> Palabra Reservada (#{fila},#{columna}) "
        else
          if buscar_identi(f)
            actual += "#{f} -> Identificador (#{fila},#{columna}) "
          else
            if buscar_numero(f)
              actual += "#{f} -> Numero (#{fila},#{columna}) "
            else
              if f=="\""
                actual += "#{f}"
                bandera = Banderas.cadena
                filastr = fila
                columnastr = columna
              elsif f=="-"
                #actual += "#{f} => Resta (#{fila},#{columna}) "
                bandera = Banderas.resta
              elsif f=="+"
                #actual += "#{f} => Suma (#{fila},#{columna}) "
                bandera = Banderas.suma
              elsif f=="*"
                actual += "#{f} -> Multiplicacion (#{fila},#{columna}) "
              elsif f=="<"
                bandera = Banderas.comparacionmen
              elsif f==">"
                bandera = Banderas.comparacionmay
              elsif f==":"
                bandera = Banderas.asigna
              elsif f=="="
                bandera = Banderas.igual
              elsif f=="!"
                bandera = Banderas.niega
              elsif f=="/"
                #actual += "#{f} => Division (#{fila},#{columna}) "
                bandera = Banderas.division
              elsif f==";" || f==","
                actual += "#{f} -> Separacion de variables (#{fila},#{columna}) "
              elsif f=="."
                actual += "#{f} -> Punto (#{fila},#{columna}) "
              elsif f=="("
                actual += "#{f} -> Parentesis que abre (#{fila},#{columna}) "
              elsif f==")"
                actual += "#{f} -> Parentesis que cierra (#{fila},#{columna}) "
              elsif f=="{"
                actual += "#{f} -> Llave que abre (#{fila},#{columna}) "
              elsif f=="}"
                actual += "#{f} -> Llave que cierra (#{fila},#{columna}) "
              elsif f=="%"
                actual += "#{f} -> Modulo (#{fila},#{columna})"
              end
            end
          end
        end
      end
      if f.length>0 && f != " "
        if actual == "" && bandera == 0 && f!=" "

          #analisislinea += "#{f} -> Error(#{fila},#{columna})\n"
          contador = 0
          resul = ""
          while(contador<f.length)
            a = f[contador,1]
            if Convenciones.letras.contains? a
              resul += "#{a}"
            end
          end
        else
          if bandera == Banderas.comentariolin || bandera==Banderas.cierremulti || bandera==Banderas.comentariomulti || bandera==Banderas.cadena
            analisislinea += actual
          else
            analisislinea += actual + "\n"
          end
          
        end
        columna += f.length
      elsif bandera == Banderas.comentariomulti || bandera == Banderas.cadena
        analisislinea += actual
      end
    }
    if bandera == Banderas.comentariolin
      analisislinea += "  -> Comentario en linea (#{fila},#{columnacoment})\n"
      bandera = 0
    end
    if analisislinea != "" 
      analisiscompleto += analisislinea
    end
    if bandera == Banderas.comentariomulti || bandera == Banderas.cadena
      analisiscompleto+="\n"
    end
  }
  if bandera != 0
    analisiscompleto += "-> error"
  end
  File.open("resultados-lexico.txt","w"){|f|
    f.write(analisiscompleto)
  }
  puts analisiscompleto
end

main
