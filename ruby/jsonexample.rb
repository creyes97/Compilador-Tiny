require 'rubygems'
require 'json'
require 'pp'
require 'representable/json'

json = File.read('flare.json')
obj = JSON.parse(json)

#pp obj

# class Tree
#     attr_accessor :children, :value
  
#     def initialize(v)
#       @value = v
#       @children = []
#     end
#   end
  
#   t = Tree.new(7)
#   t.children << Tree.new(3)
#   t.children << Tree.new(11)
  
#   t.value              # 7
#   t.children[0].value  # 3
#   t.children[1].value  # 11

# json = JSON.generate(t)

module TreeRepresenter
    include Representable::JSON
    
    property:name
    property:children

end
class Tree
    attr_accessor :name, :children
end

#tree = Tree.new.extend(TreeRepresenter).from_json(json)
tree = Tree.new

tree.to_json

