#   analizador semantico para gramatica modificada
#   Hecho en base al compilador tiny
#   Autor: Cesar Reyes

require 'rubygems'
require 'pp'
require 'json'
require_relative 'tree'
require_relative 'util'
require_relative 'tablaSimbolos'

class Analisis
    def main
        #Cargar el archivo donde se encuentra el arbol
        @archivo = ARGV[0]
        @jArbol = leer_archivo(@archivo)
        @lugar = 0
        @symbT = Tablasimbolos.new
        @Errores = ""
        @ArbolP = JSON.parse(@jArbol, object_class: Tree)[0]
        @Variables = []
        @esDecl = false
        @esAsign = false
        construirTabla(@ArbolP)
        pp @symbT
        t = checkTipo(@ArbolP)
        util = Util.new
        t2 = util.recorrerTipos(t)
        # pp t2
        # pp @Variables
        carpeta = Dir.pwd
        jsonn = t2.to_json
        File.open(carpeta + "/resultados/arbolsemant.txt","w"){|f|
            f.write("[" + jsonn + "]")
        }
        File.open(carpeta + "/resultados/erroressemantico.txt","w"){|f|
            f.write( @Errores )
        }
        File.open(carpeta + "/resultados/tabla-ha-ash.txt","w"){|f|
            f.write( @symbT.json )
        }
        
    end
    def leer_archivo(nombre)
        #abre un archivo y regresa su contenido
        archivo = File.open(nombre)
        texto = archivo.read
        archivo.close
        return texto
    end

    # Se aplica un recorrido en preorden para insertar los nodos a la tabla
    def traverseTab(t)
        unless t.nil?
            if t.tipo == "tipo"
                @esDecl = true
            end
            # Se ejecuta inicialmente el metodo que estaría heredado en preProc
            insertarNodo(t)
            t.children.each{|f|
                traverseTab(f);
            }
            # Se ejecuta aqui el metodo que estaría heredado en postProc
            nada(t)
            if t.tipo == "tipo"
                @esDecl = false
            end
        end
    end

    # Se aplica un recorrido en postOrden para validar los tipos de la tabla
    def traverseType(t,ptipo)
        unless t.nil?
            # Se ejecuta inicialmente el metodo que estaría heredado en preProc
            nada(t)
            if t.tipo == "tipo"
                case t.name
                when "integer"
                    ptipo = "entero"
                when "float"
                    ptipo = "decimal"
                when "bool"
                    ptipo = "bool"
                end
            elsif t.tipo == "asignacion"
                @esAsign = true
            end
            i=0
            al = t.children.length
            while i < al
                childs = t.children
                f = childs[i]
                aux = traverseType(f,ptipo);
                t.modChildren(i,aux)
                i+=1
            end
            # Se ejecuta aqui el metodo que estaría heredado en postProc
            at = t.attrib
            t = checkNodo(t, ptipo)
            if t.tipo == "asignacion"
                @esAsign = false
            end
        end
        return t
    end

    # No hace nada c:
    def nada(t)
        if t.nil?
            return
        else
            return
        end
    end
    
    # Se insertan los identificadores de t en la tabla de simbolos
    def insertarNodo(t)
        case t.tipo
        when "asignacion", "reservada"
            if t.tipo == "reservada" && t.name == "read"
                ch = t.children
                if @symbT.buscar(ch[0].name) == -1
                    # No esta en la tabla, es una nuva definicion
                    @symbT.insertar(ch[0].name,ch[0].linea,@lugar)
                    @lugar += 1
                else
                    # Ya esta en la tabla, ignora donde está y se agrega la linea
                    @symbT.modificar(ch[0].name,ch[0].linea,0)
                end
            end
        when "identificador"
            if @symbT.buscar(t.name) == -1 && @esDecl
                @symbT.insertar(t.name,t.linea,@lugar)

                @lugar +=1
            else
                @symbT.modificar(t.name,t.linea,0)
            end
        end
        
    end
    
    def construirTabla(t)
        traverseTab(t)
    end

    def tipoError(t,mensaje)
        @Errores += mensaje + " " + t.linea
    end

    def checkNodo(t, parentType)
        case t.tipo
        when "suma","resta","division","multiplicacion"
            ch = t.children
            vtipos = []
            vtipos << (ch[0].expType != "entero" || ch[1].expType != "decimal")
            vtipos << (ch[0].expType != "decimal" || ch[1].expType != "entero")
            vtipos << (ch[0].expType != "entero" || ch[1].expType != "entero")
            vtipos << (ch[0].expType != "decimal" || ch[1].expType != "decimal")
            if (vtipos[0] && vtipos[1] && vtipos[2] && vtipos[3])
                @Errores += "Error en linea #{ch[0].linea}, no se puede realizar operacion entre #{ch[0].tipo} y #{ch[1].tipo} \n"                    
            else
                if(!(vtipos[0] && vtipos[1]&&vtipos[3]))
                    t.setexpType("decimal")
                else
                    t.setexpType("entero")
                end
            end
            vc = -1
            att0 = ch[0].attrib
            att1 = ch[1].attrib
            case t.tipo
            when "suma"
                if att0[0] == 1 && att1[0] == 1 && t.expType == "decimal"
                    # 
                    vcal =  '%.3f' % (att0[1] + att1[1])
                    t.setValue(vcal.to_f)
                    t.hasValue()
                elsif att0[0] == 1 && att1[0] == 1 && t.expType == "entero"
                    # 
                    vcal = (att0[1] + att1[1])
                    t.setValue(vcal)
                    t.hasValue()
                elsif att0[0] == 1 && att1[0] == 1 && t.expType == "cadena"
                    vcal = (att0[1] + att1[1])
                    t.setValue(vcal)
                    t.hasValue()
                end
            when "resta"
                if att0[0] == 1 && att1[0] == 1 && t.expType == "decimal"
                    # 
                    vcal =  '%.3f' % (att0[1] - att1[1])
                    t.setValue(vcal.to_f)
                    t.hasValue()
                elsif att0[0] == 1 && att1[0] == 1 && t.expType == "entero"
                    # 
                    vcal = (att0[1] - att1[1])
                    t.setValue(vcal)
                    t.hasValue()
                end
            when "multiplicacion"
                if att0[0] == 1 && att1[0] == 1 && t.expType == "decimal"
                    # 
                    vcal =  '%.3f' % (att0[1] * att1[1])
                    t.setValue(vcal.to_f)
                    t.hasValue()
                elsif att0[0] == 1 && att1[0] == 1 && t.expType == "entero"
                    # 
                    vcal = (att0[1] * att1[1])
                    t.setValue(vcal)
                    t.hasValue()
                end
            when "division"
                if att0[0] == 1 && att1[0] == 1 && t.expType == "decimal"
                    # '%.5f' % 
                    vcal =  '%.4f' % (att0[1].to_f / att1[1].to_f)
                    t.setValue(vcal.to_f)
                    t.hasValue()
                elsif att0[0] == 1 && att1[0] == 1 && t.expType == "entero"
                    # 
                    vcal = (att0[1] / att1[1])
                    t.setValue(vcal)
                    t.hasValue()
                end
            end
        when "igual" 
            t.setexpType("bool")
            ch = t.children
            a = ch[0].attrib
            b = ch[1].attrib
            t.setValue(a[1]==b[1])
            t.hasValue()
        when "diferente" 
            t.setexpType("bool")
            ch = t.children
            a = ch[0].attrib
            b = ch[1].attrib
            t.setValue(a[1]!=b[1])
            t.hasValue()
        when"menori"
            t.setexpType("bool")
            ch = t.children
            a = ch[0].attrib
            b = ch[1].attrib
            t.setValue(a[1]<=b[1])
            t.hasValue() 
        when "menorq" 
            t.setexpType("bool")
            ch = t.children
            a = ch[0].attrib
            b = ch[1].attrib
            t.setValue(a[1]<b[1])
            t.hasValue()
        when "mayori" 
            t.setexpType("bool")
            ch = t.children
            a = ch[0].attrib
            b = ch[1].attrib
            t.setValue(a[1]>=b[1])
            t.hasValue()
        when "mayorq"
            t.setexpType("bool")
            ch = t.children
            a = ch[0].attrib
            b = ch[1].attrib
            t.setValue(a[1]>b[1])
            t.hasValue()
        when "identificador"
            if(parentType == "")
                tt = ""
                tv = 0
                vs = 0
                @Variables.each{|v|
                    if v[0] == t.name
                        tt = v[1] 
                        tv = v[2]
                        vs = v[3]
                    end
                }
                if tt==""
                    @Errores += "Error en linea #{t.linea}, la variable no esta declarada \n"
                else
                    t.setexpType(tt)
                end
                if vs == 0 && @esAsign==false
                    @Errores += "Error en linea #{t.linea}, la variable no esta inicializada \n"
                elsif vs != 0
                    t.setValue(tv)
                    t.hasValue()
                end
            else
                t.setexpType(parentType)
                e = false;
                @Variables.each{|vv|
                    if vv[0] == t.name
                        @Errores += "Error en linea #{t.linea}, la variable: #{t.name},  ya fue declarada \n"
                        e=true
                    end
                }
                if e == false
                    ax = []
                    ax << t.name
                    ax << parentType
                    ax << -1
                    ax << 0
                    @Variables << ax
                    pp @Variables
                end
            end
        when "entero"
            t.setexpType(t.tipo)
            t.setValue(t.name.to_i)
            t.hasValue()
        when "decimal"
            t.setexpType(t.tipo)
            t.setValue(t.name.to_f)
            t.hasValue()
        when "bool","cadena"
            t.setexpType(t.tipo)
            t.setValue(t.name)
            t.hasValue()
        when "reservada"
            if t.name == "if"
                ch = t.children
                if (ch[0].expType != "bool")
                    @Errores += "Error en linea #{ch[0].linea}, no se verifica con bool \n"
                end
            elsif t.name == "write"
                ch = t.children
                if ch.length>0
                    if(ch[0].expType != "entero" && ch[0].expType != "decimal" && ch[0].expType != "bool")
                        @Errores += "Error en linea #{ch[0].linea}, Escritura de un valor invalido \n"
                    end
                else
                    @Errores += "Error en linea #{t.linea}, Escritura de un valor invalido \n"
                end
                
            elsif t.name == "repeat"
                ch = t.children
                if(ch[0].expType != "bool")
                    @Errores += "Error en linea #{ch[0].linea}, Condicion de repeat no es bool \n"
                end
            end
        when "asignacion"
            ch = t.children
            if(ch[0].expType != ch[1].expType && (ch[0].expType != "decimal" && ch[1].expType != "entero"))
                @Errores += "Error en linea #{ch[0].linea}, La variable no admite valor asignado \n"
                n = t.name
                t.setname(n +" ERROR")
            else
                att0 = ch[0].attrib
                att1 = ch[1].attrib
                unless(att1[0] == 0)
                    t.setValue(att1[1])
                    t.hasValue()
                    if ch[0].expType == "decimal" && ch[1].expType == "entero"
                        ch[0].setValue(att1[1].to_f)
                    else
                        ch[0].setValue(att1[1])
                    end
                    
                    ch[0].hasValue()
                    t.modChildren(0,ch[0])
                    i=0
                    vs = @Variables.length
                    while i<vs
                        vd = @Variables[i]
                        if vd[0] == ch[0].name
                            vd[2] = att1[1]
                            @Variables[i] = vd
                            @symbT.modificarV(vd[0],att1[1])
                        end
                        i+=1
                    end
                end
                # unless(att0[1] == -1)
                #     t.setValue(att0[1])
                #     ch[1].setValue(att1[1])
                #     t.modChildren(1,ch[1])
                #     i=0
                #     vs = @Variables.length
                #     while i<vs
                #         vd = @Variables[i]
                #         if vd[0] == ch[1].name
                #             vd[2] = att1[1]
                #             @Variables[i] = vd
                #         end
                #         i+=1
                #     end
                # end
                nvv = @Variables.length
                i=0
                while i < nvv
                    v = @Variables[i]
                    if v[0] == t.name
                        ag = t.attrib
                        v[2] = ag[1]
                        @Variables[i] = v
                        @symbT.modificarV(v[0],ag[1])
                    end
                    i+=1
                end
                t.setexpType(ch[0].expType)
            end
            
        end
        
        return t
    end

    def checkTipo(arbol)
        t = Tree.new 
        t = traverseType(arbol,"")
        # pp "------------------ ARBOL CON TIPOS CHECAOS ---------------"
        # pp t
        return t
    end
end

an = Analisis.new
an.main